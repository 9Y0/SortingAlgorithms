package me._9y0.sortingalgorithms.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import me._9y0.sortingalgorithms.algorithms.ISortingAlgorithm;

public class Settings {

	public static final ObjectProperty<ISortingAlgorithm> ALGORITHM = new SimpleObjectProperty<>();
	public static final IntegerProperty DELAY = new SimpleIntegerProperty(5);
}
