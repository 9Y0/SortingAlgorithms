package me._9y0.sortingalgorithms.view;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import me._9y0.sortingalgorithms.algorithms.BubbleSort;
import me._9y0.sortingalgorithms.algorithms.ISortingAlgorithm;
import me._9y0.sortingalgorithms.algorithms.QuickSort;

public class SettingsPane extends VBox {

	private static final ISortingAlgorithm[] SORTING_ALGORITHMS = new ISortingAlgorithm[]{ //
			new BubbleSort(), //
			new QuickSort() //
	};

	public SettingsPane(VisualizationPane visualizationPane) {
		Settings.ALGORITHM.set(SORTING_ALGORITHMS[0]);

		this.setMinSize(Window.SIDEBAR_WIDTH, Window.WINDOW_HEIGHT);
		this.setMaxSize(Window.SIDEBAR_WIDTH, Window.WINDOW_HEIGHT);

		this.setAlignment(Pos.TOP_CENTER);

		this.getChildren().add(new Label("Settings"));

		Button startButton = new Button("Start");
		startButton.setOnAction(event -> visualizationPane.start());
		this.getChildren().add(startButton);

		Button stopButton = new Button("Stop");
		stopButton.setOnAction(event -> visualizationPane.stop());
		this.getChildren().add(stopButton);

		Button randomizeButton = new Button("Randomize");
		randomizeButton.setOnAction(event -> visualizationPane.randomize());
		this.getChildren().add(randomizeButton);

		ComboBox<ISortingAlgorithm> algorithmComboBox = new ComboBox<>(
				FXCollections.observableArrayList(SORTING_ALGORITHMS));
		algorithmComboBox.getSelectionModel().selectFirst();
		algorithmComboBox.valueProperty()
				.addListener((observable, oldValue, newValue) -> Settings.ALGORITHM.set(newValue));
		this.getChildren().add(algorithmComboBox);
	}
}
