package me._9y0.sortingalgorithms.view;

import java.util.concurrent.Future;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import me._9y0.sortingalgorithms.algorithms.ISortingAlgorithm;
import me._9y0.sortingalgorithms.array.SortedArray;

public class VisualizationPane extends Canvas {

	private static final int MAX_HEIGHT = Window.WINDOW_HEIGHT * 3 / 4;
	private static final int WIDTH = 5;
	private static final int RED_BARS = 9;

	private final SortedArray array;

	private Future<?> sortingTask;
	private boolean sorted = true;

	public VisualizationPane() {
		this.setWidth(Window.VISUALIZATION_WIDTH);
		this.setHeight(Window.WINDOW_HEIGHT);

		this.array = new SortedArray(Window.VISUALIZATION_WIDTH / WIDTH);
		this.array.fillSorted(MAX_HEIGHT);

		draw();
	}

	public void randomize() {
		if (!sorted || isRunning()) {
			return;
		}

		Window.THREAD_EXECUTOR.submit(() -> {
			array.fillRandom(MAX_HEIGHT, i -> {
				Platform.runLater(() -> repaint(i));
				sleep();
			}, () -> {
				Platform.runLater(this::draw);
				sorted = false;
			});
		});
	}

	public void start() {
		if (sorted || isRunning()) {
			return;
		}

		sortingTask = Window.THREAD_EXECUTOR.submit(() -> {
			ISortingAlgorithm sortingAlgorithm = Settings.ALGORITHM.get();

			sortingAlgorithm.sort(array, i -> {
				Platform.runLater(() -> repaint(i));
				sleep();
			});

			displayRedTrail();
			this.sorted = true;
		});
	}

	// TODO: There's something wrong when you stop. it al gets sorted instantly :/
	public void stop() {
		if (sortingTask != null) {
			sortingTask.cancel(true);
		}
	}

	private boolean isRunning() {
		return sortingTask != null && !sortingTask.isDone() && !sortingTask.isCancelled();
	}

	private void sleep() {
		sleep(Settings.DELAY.get());
	}

	private void displayRedTrail() {
		for (int i = 0; i < array.size() + RED_BARS; i++) {
			int j = i;
			Platform.runLater(() -> repaint(j));
			sleep(5);
		}
	}

	private void sleep(int length) {
		try {
			Thread.sleep(length);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	public void repaint(int redIndex) {
		GraphicsContext g = getGraphicsContext2D();

		background(g);

		for (int i = 0; i < array.size(); i++) {
			g.setFill(getTrailColor(i, redIndex));
			int height = array.get(i);
			g.fillRect(i * WIDTH, Window.WINDOW_HEIGHT - height, WIDTH, height);
		}
	}

	private Color getTrailColor(int currentIndex, int redIndex) {
		int diff = redIndex - currentIndex;
		if (diff < 0 || diff > RED_BARS - 1) {
			return Color.WHITE;
		}

		int gb = 0;
		for (int i = 0; i < diff; i++) {
			gb += 20;
		}
		return Color.rgb(255, gb, gb);
	}

	public void draw() {
		GraphicsContext g = getGraphicsContext2D();

		background(g);

		g.setFill(Color.WHITE);
		for (int i = 0; i < array.size(); i++) {
			int height = array.get(i);
			g.fillRect(i * WIDTH, Window.WINDOW_HEIGHT - height, WIDTH, height);
		}
	}

	private void background(GraphicsContext g) {
		g.setFill(Color.GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}
