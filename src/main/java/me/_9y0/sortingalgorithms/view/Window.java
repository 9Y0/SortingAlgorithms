package me._9y0.sortingalgorithms.view;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Window extends Application {

	public static final int VISUALIZATION_WIDTH = 1280;
	public static final int SIDEBAR_WIDTH = 200;
	public static final int WINDOW_HEIGHT = 720;

	public static final ExecutorService THREAD_EXECUTOR = Executors.newSingleThreadExecutor();

	@Override
	public void start(Stage primaryStage) {
		BorderPane pane = new BorderPane();

		VisualizationPane visualizationPane = new VisualizationPane();
		pane.setCenter(visualizationPane);
		pane.setRight(new SettingsPane(visualizationPane));

		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Sorting Algorithms");
		primaryStage.setResizable(false);

		primaryStage.show();
	}

	@Override
	public void stop() {
		THREAD_EXECUTOR.shutdownNow();
	}
}
