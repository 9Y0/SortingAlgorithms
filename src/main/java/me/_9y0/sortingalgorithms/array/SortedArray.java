package me._9y0.sortingalgorithms.array;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.function.IntConsumer;

public class SortedArray implements Iterable<Integer> {

	private static final Random RANDOM = new Random();
	private final int[] array;

	public SortedArray(int size) {
		this.array = new int[size];
	}

	public void swap(int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	@Override
	public Iterator<Integer> iterator() {
		return Arrays.stream(array).iterator();
	}

	public void fillSorted(int max) {
		double step = max / (double) size();
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) ((i + 1) * step);
		}
	}

	public void fillRandom(int max) {
		fillRandom(max, i -> {
		}, () -> {
		});
	}

	public void fillRandom(int max, IntConsumer onNewRandom, Runnable onDone) {
		for (int i = 0; i < array.length; i++) {
			array[i] = RANDOM.nextInt(max) + 1;
			onNewRandom.accept(i);
		}

		onDone.run();
	}

	public int size() {
		return array.length;
	}

	public int get(int i) {
		return array[i];
	}
}
