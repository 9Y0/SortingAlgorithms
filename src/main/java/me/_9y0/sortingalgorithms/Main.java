package me._9y0.sortingalgorithms;

import javafx.application.Application;
import me._9y0.sortingalgorithms.view.Window;

public class Main {

	public static void main(String[] args) {
		System.setProperty("sun.java2d.opengl", "true");
		Application.launch(Window.class, args);
	}
}
