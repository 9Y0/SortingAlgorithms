package me._9y0.sortingalgorithms.algorithms;

import java.util.function.IntConsumer;

import me._9y0.sortingalgorithms.array.SortedArray;

public interface ISortingAlgorithm {

	default void sort(SortedArray array) {
		sort(array, i -> {
		});
	}

	void sort(SortedArray array, IntConsumer onSwap);

	String toString();
}
