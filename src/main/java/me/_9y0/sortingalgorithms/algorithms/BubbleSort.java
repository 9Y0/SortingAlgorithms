package me._9y0.sortingalgorithms.algorithms;

import java.util.function.IntConsumer;

import me._9y0.sortingalgorithms.array.SortedArray;

public class BubbleSort implements ISortingAlgorithm {

	@Override
	public void sort(SortedArray array, IntConsumer onDraw) {
		int n = array.size();

		while (n > 0) {
			int newN = 0;
			for (int i = 1; i < n; i++) {
				if (array.get(i - 1) > array.get(i)) {
					array.swap(i - 1, i);
					newN = i;
				}
				onDraw.accept(i);
			}
			n = newN;
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
