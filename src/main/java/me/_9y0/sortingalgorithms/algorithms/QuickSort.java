package me._9y0.sortingalgorithms.algorithms;

import java.util.function.IntConsumer;

import me._9y0.sortingalgorithms.array.SortedArray;

public class QuickSort implements ISortingAlgorithm {

	@Override
	public void sort(SortedArray array, IntConsumer onDraw) {
		quickSort(array, 0, array.size() - 1, onDraw);
	}

	private void quickSort(SortedArray array, int low, int high, IntConsumer onDraw) {
		if (low < high) {
			int p = partition(array, low, high, onDraw);
			quickSort(array, low, p - 1, onDraw);
			quickSort(array, p + 1, high, onDraw);
		}
	}

	private int partition(SortedArray array, int low, int high, IntConsumer onDraw) {
		int pivot = array.get(high);
		int i = low;
		for (int j = low; j < high; j++) {
			if (array.get(j) < pivot) {
				array.swap(i, j);
				i++;
				onDraw.accept(i);
			}
		}
		array.swap(i, high);
		onDraw.accept(i);
		return i;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
